///
//  Generated code. Do not modify.
//  source: home.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'home.pb.dart' as $0;
export 'home.pb.dart';

class HomeClient extends $grpc.Client {
  static final _$performanceOfTheDay = $grpc.ClientMethod<
          $0.QuantityOfTheDayRequest, $0.QuantityOfTheDayResponse>(
      '/home.Home/PerformanceOfTheDay',
      ($0.QuantityOfTheDayRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.QuantityOfTheDayResponse.fromBuffer(value));
  static final _$orderVolumeOfTheDayFMC =
      $grpc.ClientMethod<$0.OrderVolumeRequest, $0.OrderVolumeResponse>(
          '/home.Home/OrderVolumeOfTheDayFMC',
          ($0.OrderVolumeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.OrderVolumeResponse.fromBuffer(value));

  HomeClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.QuantityOfTheDayResponse> performanceOfTheDay(
      $0.QuantityOfTheDayRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$performanceOfTheDay, request, options: options);
  }

  $grpc.ResponseFuture<$0.OrderVolumeResponse> orderVolumeOfTheDayFMC(
      $0.OrderVolumeRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$orderVolumeOfTheDayFMC, request,
        options: options);
  }
}

abstract class HomeServiceBase extends $grpc.Service {
  $core.String get $name => 'home.Home';

  HomeServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.QuantityOfTheDayRequest,
            $0.QuantityOfTheDayResponse>(
        'PerformanceOfTheDay',
        performanceOfTheDay_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.QuantityOfTheDayRequest.fromBuffer(value),
        ($0.QuantityOfTheDayResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.OrderVolumeRequest, $0.OrderVolumeResponse>(
            'OrderVolumeOfTheDayFMC',
            orderVolumeOfTheDayFMC_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.OrderVolumeRequest.fromBuffer(value),
            ($0.OrderVolumeResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.QuantityOfTheDayResponse> performanceOfTheDay_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.QuantityOfTheDayRequest> request) async {
    return performanceOfTheDay(call, await request);
  }

  $async.Future<$0.OrderVolumeResponse> orderVolumeOfTheDayFMC_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.OrderVolumeRequest> request) async {
    return orderVolumeOfTheDayFMC(call, await request);
  }

  $async.Future<$0.QuantityOfTheDayResponse> performanceOfTheDay(
      $grpc.ServiceCall call, $0.QuantityOfTheDayRequest request);
  $async.Future<$0.OrderVolumeResponse> orderVolumeOfTheDayFMC(
      $grpc.ServiceCall call, $0.OrderVolumeRequest request);
}
