///
//  Generated code. Do not modify.
//  source: home.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class OrderVolumeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderVolumeRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'home'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user')
    ..hasRequiredFields = false
  ;

  OrderVolumeRequest._() : super();
  factory OrderVolumeRequest({
    $core.String? user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory OrderVolumeRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderVolumeRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderVolumeRequest clone() => OrderVolumeRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderVolumeRequest copyWith(void Function(OrderVolumeRequest) updates) => super.copyWith((message) => updates(message as OrderVolumeRequest)) as OrderVolumeRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderVolumeRequest create() => OrderVolumeRequest._();
  OrderVolumeRequest createEmptyInstance() => create();
  static $pb.PbList<OrderVolumeRequest> createRepeated() => $pb.PbList<OrderVolumeRequest>();
  @$core.pragma('dart2js:noInline')
  static OrderVolumeRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderVolumeRequest>(create);
  static OrderVolumeRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get user => $_getSZ(0);
  @$pb.TagNumber(1)
  set user($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
}

class OrderVolumeResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderVolumeResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'home'), createEmptyInstance: create)
    ..pc<OrderValumeOfTheday>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: OrderValumeOfTheday.create)
    ..hasRequiredFields = false
  ;

  OrderVolumeResponse._() : super();
  factory OrderVolumeResponse({
    $core.Iterable<OrderValumeOfTheday>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory OrderVolumeResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderVolumeResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderVolumeResponse clone() => OrderVolumeResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderVolumeResponse copyWith(void Function(OrderVolumeResponse) updates) => super.copyWith((message) => updates(message as OrderVolumeResponse)) as OrderVolumeResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderVolumeResponse create() => OrderVolumeResponse._();
  OrderVolumeResponse createEmptyInstance() => create();
  static $pb.PbList<OrderVolumeResponse> createRepeated() => $pb.PbList<OrderVolumeResponse>();
  @$core.pragma('dart2js:noInline')
  static OrderVolumeResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderVolumeResponse>(create);
  static OrderVolumeResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<OrderValumeOfTheday> get items => $_getList(0);
}

class OrderValumeOfTheday extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderValumeOfTheday', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'home'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'realized', $pb.PbFieldType.OF)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'target', $pb.PbFieldType.OF)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'notRealized', $pb.PbFieldType.OF, protoName: 'notRealized')
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'percent', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  OrderValumeOfTheday._() : super();
  factory OrderValumeOfTheday({
    $core.String? title,
    $core.double? realized,
    $core.double? target,
    $core.double? notRealized,
    $core.double? percent,
  }) {
    final _result = create();
    if (title != null) {
      _result.title = title;
    }
    if (realized != null) {
      _result.realized = realized;
    }
    if (target != null) {
      _result.target = target;
    }
    if (notRealized != null) {
      _result.notRealized = notRealized;
    }
    if (percent != null) {
      _result.percent = percent;
    }
    return _result;
  }
  factory OrderValumeOfTheday.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderValumeOfTheday.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderValumeOfTheday clone() => OrderValumeOfTheday()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderValumeOfTheday copyWith(void Function(OrderValumeOfTheday) updates) => super.copyWith((message) => updates(message as OrderValumeOfTheday)) as OrderValumeOfTheday; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderValumeOfTheday create() => OrderValumeOfTheday._();
  OrderValumeOfTheday createEmptyInstance() => create();
  static $pb.PbList<OrderValumeOfTheday> createRepeated() => $pb.PbList<OrderValumeOfTheday>();
  @$core.pragma('dart2js:noInline')
  static OrderValumeOfTheday getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderValumeOfTheday>(create);
  static OrderValumeOfTheday? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get title => $_getSZ(0);
  @$pb.TagNumber(1)
  set title($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTitle() => $_has(0);
  @$pb.TagNumber(1)
  void clearTitle() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get realized => $_getN(1);
  @$pb.TagNumber(2)
  set realized($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRealized() => $_has(1);
  @$pb.TagNumber(2)
  void clearRealized() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get target => $_getN(2);
  @$pb.TagNumber(3)
  set target($core.double v) { $_setFloat(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTarget() => $_has(2);
  @$pb.TagNumber(3)
  void clearTarget() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get notRealized => $_getN(3);
  @$pb.TagNumber(4)
  set notRealized($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasNotRealized() => $_has(3);
  @$pb.TagNumber(4)
  void clearNotRealized() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get percent => $_getN(4);
  @$pb.TagNumber(5)
  set percent($core.double v) { $_setFloat(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPercent() => $_has(4);
  @$pb.TagNumber(5)
  void clearPercent() => clearField(5);
}

class QuantityOfTheDayRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuantityOfTheDayRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'home'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user')
    ..hasRequiredFields = false
  ;

  QuantityOfTheDayRequest._() : super();
  factory QuantityOfTheDayRequest({
    $core.String? user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory QuantityOfTheDayRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuantityOfTheDayRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuantityOfTheDayRequest clone() => QuantityOfTheDayRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuantityOfTheDayRequest copyWith(void Function(QuantityOfTheDayRequest) updates) => super.copyWith((message) => updates(message as QuantityOfTheDayRequest)) as QuantityOfTheDayRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuantityOfTheDayRequest create() => QuantityOfTheDayRequest._();
  QuantityOfTheDayRequest createEmptyInstance() => create();
  static $pb.PbList<QuantityOfTheDayRequest> createRepeated() => $pb.PbList<QuantityOfTheDayRequest>();
  @$core.pragma('dart2js:noInline')
  static QuantityOfTheDayRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuantityOfTheDayRequest>(create);
  static QuantityOfTheDayRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get user => $_getSZ(0);
  @$pb.TagNumber(1)
  set user($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
}

class QuantityOfTheDayResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuantityOfTheDayResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'home'), createEmptyInstance: create)
    ..pc<QuantityOfTheDay>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: QuantityOfTheDay.create)
    ..hasRequiredFields = false
  ;

  QuantityOfTheDayResponse._() : super();
  factory QuantityOfTheDayResponse({
    $core.Iterable<QuantityOfTheDay>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory QuantityOfTheDayResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuantityOfTheDayResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuantityOfTheDayResponse clone() => QuantityOfTheDayResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuantityOfTheDayResponse copyWith(void Function(QuantityOfTheDayResponse) updates) => super.copyWith((message) => updates(message as QuantityOfTheDayResponse)) as QuantityOfTheDayResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuantityOfTheDayResponse create() => QuantityOfTheDayResponse._();
  QuantityOfTheDayResponse createEmptyInstance() => create();
  static $pb.PbList<QuantityOfTheDayResponse> createRepeated() => $pb.PbList<QuantityOfTheDayResponse>();
  @$core.pragma('dart2js:noInline')
  static QuantityOfTheDayResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuantityOfTheDayResponse>(create);
  static QuantityOfTheDayResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<QuantityOfTheDay> get items => $_getList(0);
}

class QuantityOfTheDay extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QuantityOfTheDay', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'home'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'realized', $pb.PbFieldType.OF)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'target', $pb.PbFieldType.OF)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'notRealized', $pb.PbFieldType.OF, protoName: 'notRealized')
    ..a<$core.double>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'percent', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  QuantityOfTheDay._() : super();
  factory QuantityOfTheDay({
    $core.String? title,
    $core.double? realized,
    $core.double? target,
    $core.double? notRealized,
    $core.double? percent,
  }) {
    final _result = create();
    if (title != null) {
      _result.title = title;
    }
    if (realized != null) {
      _result.realized = realized;
    }
    if (target != null) {
      _result.target = target;
    }
    if (notRealized != null) {
      _result.notRealized = notRealized;
    }
    if (percent != null) {
      _result.percent = percent;
    }
    return _result;
  }
  factory QuantityOfTheDay.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QuantityOfTheDay.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QuantityOfTheDay clone() => QuantityOfTheDay()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QuantityOfTheDay copyWith(void Function(QuantityOfTheDay) updates) => super.copyWith((message) => updates(message as QuantityOfTheDay)) as QuantityOfTheDay; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QuantityOfTheDay create() => QuantityOfTheDay._();
  QuantityOfTheDay createEmptyInstance() => create();
  static $pb.PbList<QuantityOfTheDay> createRepeated() => $pb.PbList<QuantityOfTheDay>();
  @$core.pragma('dart2js:noInline')
  static QuantityOfTheDay getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QuantityOfTheDay>(create);
  static QuantityOfTheDay? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get title => $_getSZ(0);
  @$pb.TagNumber(1)
  set title($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTitle() => $_has(0);
  @$pb.TagNumber(1)
  void clearTitle() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get realized => $_getN(1);
  @$pb.TagNumber(2)
  set realized($core.double v) { $_setFloat(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRealized() => $_has(1);
  @$pb.TagNumber(2)
  void clearRealized() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get target => $_getN(2);
  @$pb.TagNumber(3)
  set target($core.double v) { $_setFloat(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTarget() => $_has(2);
  @$pb.TagNumber(3)
  void clearTarget() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get notRealized => $_getN(3);
  @$pb.TagNumber(4)
  set notRealized($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasNotRealized() => $_has(3);
  @$pb.TagNumber(4)
  void clearNotRealized() => clearField(4);

  @$pb.TagNumber(5)
  $core.double get percent => $_getN(4);
  @$pb.TagNumber(5)
  set percent($core.double v) { $_setFloat(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPercent() => $_has(4);
  @$pb.TagNumber(5)
  void clearPercent() => clearField(5);
}

