///
//  Generated code. Do not modify.
//  source: home.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use orderVolumeRequestDescriptor instead')
const OrderVolumeRequest$json = const {
  '1': 'OrderVolumeRequest',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 9, '10': 'user'},
  ],
};

/// Descriptor for `OrderVolumeRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderVolumeRequestDescriptor = $convert.base64Decode('ChJPcmRlclZvbHVtZVJlcXVlc3QSEgoEdXNlchgBIAEoCVIEdXNlcg==');
@$core.Deprecated('Use orderVolumeResponseDescriptor instead')
const OrderVolumeResponse$json = const {
  '1': 'OrderVolumeResponse',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.home.OrderValumeOfTheday', '10': 'items'},
  ],
};

/// Descriptor for `OrderVolumeResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderVolumeResponseDescriptor = $convert.base64Decode('ChNPcmRlclZvbHVtZVJlc3BvbnNlEi8KBWl0ZW1zGAEgAygLMhkuaG9tZS5PcmRlclZhbHVtZU9mVGhlZGF5UgVpdGVtcw==');
@$core.Deprecated('Use orderValumeOfThedayDescriptor instead')
const OrderValumeOfTheday$json = const {
  '1': 'OrderValumeOfTheday',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'realized', '3': 2, '4': 1, '5': 2, '10': 'realized'},
    const {'1': 'target', '3': 3, '4': 1, '5': 2, '10': 'target'},
    const {'1': 'notRealized', '3': 4, '4': 1, '5': 2, '10': 'notRealized'},
    const {'1': 'percent', '3': 5, '4': 1, '5': 2, '10': 'percent'},
  ],
};

/// Descriptor for `OrderValumeOfTheday`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderValumeOfThedayDescriptor = $convert.base64Decode('ChNPcmRlclZhbHVtZU9mVGhlZGF5EhQKBXRpdGxlGAEgASgJUgV0aXRsZRIaCghyZWFsaXplZBgCIAEoAlIIcmVhbGl6ZWQSFgoGdGFyZ2V0GAMgASgCUgZ0YXJnZXQSIAoLbm90UmVhbGl6ZWQYBCABKAJSC25vdFJlYWxpemVkEhgKB3BlcmNlbnQYBSABKAJSB3BlcmNlbnQ=');
@$core.Deprecated('Use quantityOfTheDayRequestDescriptor instead')
const QuantityOfTheDayRequest$json = const {
  '1': 'QuantityOfTheDayRequest',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 9, '10': 'user'},
  ],
};

/// Descriptor for `QuantityOfTheDayRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List quantityOfTheDayRequestDescriptor = $convert.base64Decode('ChdRdWFudGl0eU9mVGhlRGF5UmVxdWVzdBISCgR1c2VyGAEgASgJUgR1c2Vy');
@$core.Deprecated('Use quantityOfTheDayResponseDescriptor instead')
const QuantityOfTheDayResponse$json = const {
  '1': 'QuantityOfTheDayResponse',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.home.QuantityOfTheDay', '10': 'items'},
  ],
};

/// Descriptor for `QuantityOfTheDayResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List quantityOfTheDayResponseDescriptor = $convert.base64Decode('ChhRdWFudGl0eU9mVGhlRGF5UmVzcG9uc2USLAoFaXRlbXMYASADKAsyFi5ob21lLlF1YW50aXR5T2ZUaGVEYXlSBWl0ZW1z');
@$core.Deprecated('Use quantityOfTheDayDescriptor instead')
const QuantityOfTheDay$json = const {
  '1': 'QuantityOfTheDay',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'realized', '3': 2, '4': 1, '5': 2, '10': 'realized'},
    const {'1': 'target', '3': 3, '4': 1, '5': 2, '10': 'target'},
    const {'1': 'notRealized', '3': 4, '4': 1, '5': 2, '10': 'notRealized'},
    const {'1': 'percent', '3': 5, '4': 1, '5': 2, '10': 'percent'},
  ],
};

/// Descriptor for `QuantityOfTheDay`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List quantityOfTheDayDescriptor = $convert.base64Decode('ChBRdWFudGl0eU9mVGhlRGF5EhQKBXRpdGxlGAEgASgJUgV0aXRsZRIaCghyZWFsaXplZBgCIAEoAlIIcmVhbGl6ZWQSFgoGdGFyZ2V0GAMgASgCUgZ0YXJnZXQSIAoLbm90UmVhbGl6ZWQYBCABKAJSC25vdFJlYWxpemVkEhgKB3BlcmNlbnQYBSABKAJSB3BlcmNlbnQ=');
