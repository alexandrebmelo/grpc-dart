///
//  Generated code. Do not modify.
//  source: authsalesrep.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use signInRequestDescriptor instead')
const SignInRequest$json = const {
  '1': 'SignInRequest',
  '2': const [
    const {'1': 'username', '3': 1, '4': 1, '5': 9, '10': 'username'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `SignInRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List signInRequestDescriptor = $convert.base64Decode('Cg1TaWduSW5SZXF1ZXN0EhoKCHVzZXJuYW1lGAEgASgJUgh1c2VybmFtZRIaCghwYXNzd29yZBgCIAEoCVIIcGFzc3dvcmQ=');
@$core.Deprecated('Use signInResponseDescriptor instead')
const SignInResponse$json = const {
  '1': 'SignInResponse',
  '2': const [
    const {'1': 'user', '3': 1, '4': 3, '5': 11, '6': '.authsalesrep.SignInResponse.UserEntry', '10': 'user'},
  ],
  '3': const [SignInResponse_UserEntry$json],
};

@$core.Deprecated('Use signInResponseDescriptor instead')
const SignInResponse_UserEntry$json = const {
  '1': 'UserEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 11, '6': '.authsalesrep.UserSalesRep', '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `SignInResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List signInResponseDescriptor = $convert.base64Decode('Cg5TaWduSW5SZXNwb25zZRI6CgR1c2VyGAEgAygLMiYuYXV0aHNhbGVzcmVwLlNpZ25JblJlc3BvbnNlLlVzZXJFbnRyeVIEdXNlchpTCglVc2VyRW50cnkSEAoDa2V5GAEgASgJUgNrZXkSMAoFdmFsdWUYAiABKAsyGi5hdXRoc2FsZXNyZXAuVXNlclNhbGVzUmVwUgV2YWx1ZToCOAE=');
@$core.Deprecated('Use userSalesRepDescriptor instead')
const UserSalesRep$json = const {
  '1': 'UserSalesRep',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'sid', '3': 3, '4': 1, '5': 9, '10': 'sid'},
    const {'1': 'password', '3': 4, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'occupation', '3': 5, '4': 1, '5': 9, '10': 'occupation'},
    const {'1': 'phoneNumber', '3': 6, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'b2bStatus', '3': 7, '4': 1, '5': 9, '10': 'b2bStatus'},
    const {'1': 'birthDate', '3': 8, '4': 1, '5': 9, '10': 'birthDate'},
  ],
};

/// Descriptor for `UserSalesRep`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userSalesRepDescriptor = $convert.base64Decode('CgxVc2VyU2FsZXNSZXASEgoEbmFtZRgBIAEoCVIEbmFtZRIUCgVlbWFpbBgCIAEoCVIFZW1haWwSEAoDc2lkGAMgASgJUgNzaWQSGgoIcGFzc3dvcmQYBCABKAlSCHBhc3N3b3JkEh4KCm9jY3VwYXRpb24YBSABKAlSCm9jY3VwYXRpb24SIAoLcGhvbmVOdW1iZXIYBiABKAlSC3Bob25lTnVtYmVyEhwKCWIyYlN0YXR1cxgHIAEoCVIJYjJiU3RhdHVzEhwKCWJpcnRoRGF0ZRgIIAEoCVIJYmlydGhEYXRl');
