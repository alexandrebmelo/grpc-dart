///
//  Generated code. Do not modify.
//  source: authsalesrep.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'authsalesrep.pb.dart' as $0;
export 'authsalesrep.pb.dart';

class AuthSalesRepClient extends $grpc.Client {
  static final _$signInSalesRep =
      $grpc.ClientMethod<$0.SignInRequest, $0.SignInResponse>(
          '/authsalesrep.AuthSalesRep/signInSalesRep',
          ($0.SignInRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.SignInResponse.fromBuffer(value));

  AuthSalesRepClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.SignInResponse> signInSalesRep(
      $0.SignInRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$signInSalesRep, request, options: options);
  }
}

abstract class AuthSalesRepServiceBase extends $grpc.Service {
  $core.String get $name => 'authsalesrep.AuthSalesRep';

  AuthSalesRepServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.SignInRequest, $0.SignInResponse>(
        'signInSalesRep',
        signInSalesRep_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SignInRequest.fromBuffer(value),
        ($0.SignInResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.SignInResponse> signInSalesRep_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SignInRequest> request) async {
    return signInSalesRep(call, await request);
  }

  $async.Future<$0.SignInResponse> signInSalesRep(
      $grpc.ServiceCall call, $0.SignInRequest request);
}
