///
//  Generated code. Do not modify.
//  source: tracking.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use trackingRequestDescriptor instead')
const TrackingRequest$json = const {
  '1': 'TrackingRequest',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 9, '10': 'user'},
  ],
};

/// Descriptor for `TrackingRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List trackingRequestDescriptor = $convert.base64Decode('Cg9UcmFja2luZ1JlcXVlc3QSEgoEdXNlchgBIAEoCVIEdXNlcg==');
@$core.Deprecated('Use trackingResponseDescriptor instead')
const TrackingResponse$json = const {
  '1': 'TrackingResponse',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.tracking.Tracking', '10': 'items'},
  ],
};

/// Descriptor for `TrackingResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List trackingResponseDescriptor = $convert.base64Decode('ChBUcmFja2luZ1Jlc3BvbnNlEigKBWl0ZW1zGAEgAygLMhIudHJhY2tpbmcuVHJhY2tpbmdSBWl0ZW1z');
@$core.Deprecated('Use trackingDescriptor instead')
const Tracking$json = const {
  '1': 'Tracking',
  '2': const [
    const {'1': 'date', '3': 1, '4': 1, '5': 9, '10': 'date'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'nf', '3': 3, '4': 1, '5': 9, '10': 'nf'},
    const {'1': 'status', '3': 4, '4': 1, '5': 9, '10': 'status'},
    const {'1': 'Updated', '3': 5, '4': 1, '5': 9, '10': 'Updated'},
    const {'1': 'imageLink', '3': 6, '4': 1, '5': 9, '10': 'imageLink'},
    const {'1': 'quantity', '3': 7, '4': 1, '5': 9, '10': 'quantity'},
    const {'1': 'historyList', '3': 8, '4': 3, '5': 11, '6': '.tracking.DetailedHistory', '10': 'historyList'},
  ],
};

/// Descriptor for `Tracking`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List trackingDescriptor = $convert.base64Decode('CghUcmFja2luZxISCgRkYXRlGAEgASgJUgRkYXRlEhIKBG5hbWUYAiABKAlSBG5hbWUSDgoCbmYYAyABKAlSAm5mEhYKBnN0YXR1cxgEIAEoCVIGc3RhdHVzEhgKB1VwZGF0ZWQYBSABKAlSB1VwZGF0ZWQSHAoJaW1hZ2VMaW5rGAYgASgJUglpbWFnZUxpbmsSGgoIcXVhbnRpdHkYByABKAlSCHF1YW50aXR5EjsKC2hpc3RvcnlMaXN0GAggAygLMhkudHJhY2tpbmcuRGV0YWlsZWRIaXN0b3J5UgtoaXN0b3J5TGlzdA==');
@$core.Deprecated('Use detailedHistoryDescriptor instead')
const DetailedHistory$json = const {
  '1': 'DetailedHistory',
  '2': const [
    const {'1': 'date', '3': 1, '4': 1, '5': 9, '10': 'date'},
    const {'1': 'status', '3': 2, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `DetailedHistory`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List detailedHistoryDescriptor = $convert.base64Decode('Cg9EZXRhaWxlZEhpc3RvcnkSEgoEZGF0ZRgBIAEoCVIEZGF0ZRIWCgZzdGF0dXMYAiABKAlSBnN0YXR1cw==');
