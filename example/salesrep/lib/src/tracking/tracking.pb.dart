///
//  Generated code. Do not modify.
//  source: tracking.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class TrackingRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TrackingRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'tracking'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user')
    ..hasRequiredFields = false
  ;

  TrackingRequest._() : super();
  factory TrackingRequest({
    $core.String? user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory TrackingRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TrackingRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TrackingRequest clone() => TrackingRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TrackingRequest copyWith(void Function(TrackingRequest) updates) => super.copyWith((message) => updates(message as TrackingRequest)) as TrackingRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TrackingRequest create() => TrackingRequest._();
  TrackingRequest createEmptyInstance() => create();
  static $pb.PbList<TrackingRequest> createRepeated() => $pb.PbList<TrackingRequest>();
  @$core.pragma('dart2js:noInline')
  static TrackingRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TrackingRequest>(create);
  static TrackingRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get user => $_getSZ(0);
  @$pb.TagNumber(1)
  set user($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
}

class TrackingResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'TrackingResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'tracking'), createEmptyInstance: create)
    ..pc<Tracking>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: Tracking.create)
    ..hasRequiredFields = false
  ;

  TrackingResponse._() : super();
  factory TrackingResponse({
    $core.Iterable<Tracking>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory TrackingResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TrackingResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  TrackingResponse clone() => TrackingResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  TrackingResponse copyWith(void Function(TrackingResponse) updates) => super.copyWith((message) => updates(message as TrackingResponse)) as TrackingResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TrackingResponse create() => TrackingResponse._();
  TrackingResponse createEmptyInstance() => create();
  static $pb.PbList<TrackingResponse> createRepeated() => $pb.PbList<TrackingResponse>();
  @$core.pragma('dart2js:noInline')
  static TrackingResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TrackingResponse>(create);
  static TrackingResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Tracking> get items => $_getList(0);
}

class Tracking extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Tracking', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'tracking'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'date')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nf')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Updated', protoName: 'Updated')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageLink', protoName: 'imageLink')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'quantity')
    ..pc<DetailedHistory>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'historyList', $pb.PbFieldType.PM, protoName: 'historyList', subBuilder: DetailedHistory.create)
    ..hasRequiredFields = false
  ;

  Tracking._() : super();
  factory Tracking({
    $core.String? date,
    $core.String? name,
    $core.String? nf,
    $core.String? status,
    $core.String? updated,
    $core.String? imageLink,
    $core.String? quantity,
    $core.Iterable<DetailedHistory>? historyList,
  }) {
    final _result = create();
    if (date != null) {
      _result.date = date;
    }
    if (name != null) {
      _result.name = name;
    }
    if (nf != null) {
      _result.nf = nf;
    }
    if (status != null) {
      _result.status = status;
    }
    if (updated != null) {
      _result.updated = updated;
    }
    if (imageLink != null) {
      _result.imageLink = imageLink;
    }
    if (quantity != null) {
      _result.quantity = quantity;
    }
    if (historyList != null) {
      _result.historyList.addAll(historyList);
    }
    return _result;
  }
  factory Tracking.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Tracking.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Tracking clone() => Tracking()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Tracking copyWith(void Function(Tracking) updates) => super.copyWith((message) => updates(message as Tracking)) as Tracking; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Tracking create() => Tracking._();
  Tracking createEmptyInstance() => create();
  static $pb.PbList<Tracking> createRepeated() => $pb.PbList<Tracking>();
  @$core.pragma('dart2js:noInline')
  static Tracking getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Tracking>(create);
  static Tracking? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get date => $_getSZ(0);
  @$pb.TagNumber(1)
  set date($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDate() => $_has(0);
  @$pb.TagNumber(1)
  void clearDate() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get nf => $_getSZ(2);
  @$pb.TagNumber(3)
  set nf($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasNf() => $_has(2);
  @$pb.TagNumber(3)
  void clearNf() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get status => $_getSZ(3);
  @$pb.TagNumber(4)
  set status($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasStatus() => $_has(3);
  @$pb.TagNumber(4)
  void clearStatus() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get updated => $_getSZ(4);
  @$pb.TagNumber(5)
  set updated($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasUpdated() => $_has(4);
  @$pb.TagNumber(5)
  void clearUpdated() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get imageLink => $_getSZ(5);
  @$pb.TagNumber(6)
  set imageLink($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasImageLink() => $_has(5);
  @$pb.TagNumber(6)
  void clearImageLink() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get quantity => $_getSZ(6);
  @$pb.TagNumber(7)
  set quantity($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasQuantity() => $_has(6);
  @$pb.TagNumber(7)
  void clearQuantity() => clearField(7);

  @$pb.TagNumber(8)
  $core.List<DetailedHistory> get historyList => $_getList(7);
}

class DetailedHistory extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DetailedHistory', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'tracking'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'date')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status')
    ..hasRequiredFields = false
  ;

  DetailedHistory._() : super();
  factory DetailedHistory({
    $core.String? date,
    $core.String? status,
  }) {
    final _result = create();
    if (date != null) {
      _result.date = date;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory DetailedHistory.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DetailedHistory.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DetailedHistory clone() => DetailedHistory()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DetailedHistory copyWith(void Function(DetailedHistory) updates) => super.copyWith((message) => updates(message as DetailedHistory)) as DetailedHistory; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DetailedHistory create() => DetailedHistory._();
  DetailedHistory createEmptyInstance() => create();
  static $pb.PbList<DetailedHistory> createRepeated() => $pb.PbList<DetailedHistory>();
  @$core.pragma('dart2js:noInline')
  static DetailedHistory getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DetailedHistory>(create);
  static DetailedHistory? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get date => $_getSZ(0);
  @$pb.TagNumber(1)
  set date($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDate() => $_has(0);
  @$pb.TagNumber(1)
  void clearDate() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get status => $_getSZ(1);
  @$pb.TagNumber(2)
  set status($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasStatus() => $_has(1);
  @$pb.TagNumber(2)
  void clearStatus() => clearField(2);
}

