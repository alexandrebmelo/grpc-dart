///
//  Generated code. Do not modify.
//  source: tracking.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'tracking.pb.dart' as $0;
export 'tracking.pb.dart';

class CisTrackingClient extends $grpc.Client {
  static final _$cis =
      $grpc.ClientMethod<$0.TrackingRequest, $0.TrackingResponse>(
          '/tracking.CisTracking/Cis',
          ($0.TrackingRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.TrackingResponse.fromBuffer(value));

  CisTrackingClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.TrackingResponse> cis($0.TrackingRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$cis, request, options: options);
  }
}

abstract class CisTrackingServiceBase extends $grpc.Service {
  $core.String get $name => 'tracking.CisTracking';

  CisTrackingServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.TrackingRequest, $0.TrackingResponse>(
        'Cis',
        cis_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TrackingRequest.fromBuffer(value),
        ($0.TrackingResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.TrackingResponse> cis_Pre(
      $grpc.ServiceCall call, $async.Future<$0.TrackingRequest> request) async {
    return cis(call, await request);
  }

  $async.Future<$0.TrackingResponse> cis(
      $grpc.ServiceCall call, $0.TrackingRequest request);
}
