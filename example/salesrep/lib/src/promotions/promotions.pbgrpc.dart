///
//  Generated code. Do not modify.
//  source: promotions.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'promotions.pb.dart' as $0;
export 'promotions.pb.dart';

class PromotionsClient extends $grpc.Client {
  static final _$packSC = $grpc.ClientMethod<$0.PackRequest, $0.PackResponse>(
      '/promotions.Promotions/PackSC',
      ($0.PackRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.PackResponse.fromBuffer(value));

  PromotionsClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.PackResponse> packSC($0.PackRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$packSC, request, options: options);
  }
}

abstract class PromotionsServiceBase extends $grpc.Service {
  $core.String get $name => 'promotions.Promotions';

  PromotionsServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.PackRequest, $0.PackResponse>(
        'PackSC',
        packSC_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.PackRequest.fromBuffer(value),
        ($0.PackResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.PackResponse> packSC_Pre(
      $grpc.ServiceCall call, $async.Future<$0.PackRequest> request) async {
    return packSC(call, await request);
  }

  $async.Future<$0.PackResponse> packSC(
      $grpc.ServiceCall call, $0.PackRequest request);
}
