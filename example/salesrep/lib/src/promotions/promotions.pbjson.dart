///
//  Generated code. Do not modify.
//  source: promotions.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use packRequestDescriptor instead')
const PackRequest$json = const {
  '1': 'PackRequest',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 9, '10': 'user'},
  ],
};

/// Descriptor for `PackRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packRequestDescriptor = $convert.base64Decode('CgtQYWNrUmVxdWVzdBISCgR1c2VyGAEgASgJUgR1c2Vy');
@$core.Deprecated('Use packResponseDescriptor instead')
const PackResponse$json = const {
  '1': 'PackResponse',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.promotions.Pack', '10': 'items'},
  ],
};

/// Descriptor for `PackResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packResponseDescriptor = $convert.base64Decode('CgxQYWNrUmVzcG9uc2USJgoFaXRlbXMYASADKAsyEC5wcm9tb3Rpb25zLlBhY2tSBWl0ZW1z');
@$core.Deprecated('Use packDescriptor instead')
const Pack$json = const {
  '1': 'Pack',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'imageLink', '3': 3, '4': 1, '5': 9, '10': 'imageLink'},
  ],
};

/// Descriptor for `Pack`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List packDescriptor = $convert.base64Decode('CgRQYWNrEg4KAmlkGAEgASgFUgJpZBISCgRuYW1lGAIgASgJUgRuYW1lEhwKCWltYWdlTGluaxgDIAEoCVIJaW1hZ2VMaW5r');
