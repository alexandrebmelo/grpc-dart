///
//  Generated code. Do not modify.
//  source: visitroute.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use routeRequestDescriptor instead')
const RouteRequest$json = const {
  '1': 'RouteRequest',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 9, '10': 'user'},
  ],
};

/// Descriptor for `RouteRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routeRequestDescriptor = $convert.base64Decode('CgxSb3V0ZVJlcXVlc3QSEgoEdXNlchgBIAEoCVIEdXNlcg==');
@$core.Deprecated('Use routeResponseDescriptor instead')
const RouteResponse$json = const {
  '1': 'RouteResponse',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.visitroute.Visit', '10': 'items'},
  ],
};

/// Descriptor for `RouteResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List routeResponseDescriptor = $convert.base64Decode('Cg1Sb3V0ZVJlc3BvbnNlEicKBWl0ZW1zGAEgAygLMhEudmlzaXRyb3V0ZS5WaXNpdFIFaXRlbXM=');
@$core.Deprecated('Use visitDescriptor instead')
const Visit$json = const {
  '1': 'Visit',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'label', '3': 2, '4': 1, '5': 9, '10': 'label'},
    const {'1': 'target', '3': 3, '4': 1, '5': 9, '10': 'target'},
    const {'1': 'code', '3': 4, '4': 1, '5': 9, '10': 'code'},
    const {'1': 'lock', '3': 5, '4': 1, '5': 9, '10': 'lock'},
    const {'1': 'type', '3': 6, '4': 1, '5': 9, '10': 'type'},
    const {'1': 'tag', '3': 7, '4': 1, '5': 9, '10': 'tag'},
    const {'1': 'real_day', '3': 8, '4': 1, '5': 9, '10': 'realDay'},
  ],
};

/// Descriptor for `Visit`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List visitDescriptor = $convert.base64Decode('CgVWaXNpdBISCgRuYW1lGAEgASgJUgRuYW1lEhQKBWxhYmVsGAIgASgJUgVsYWJlbBIWCgZ0YXJnZXQYAyABKAlSBnRhcmdldBISCgRjb2RlGAQgASgJUgRjb2RlEhIKBGxvY2sYBSABKAlSBGxvY2sSEgoEdHlwZRgGIAEoCVIEdHlwZRIQCgN0YWcYByABKAlSA3RhZxIZCghyZWFsX2RheRgIIAEoCVIHcmVhbERheQ==');
