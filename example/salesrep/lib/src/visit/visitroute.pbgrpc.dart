///
//  Generated code. Do not modify.
//  source: visitroute.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'visitroute.pb.dart' as $0;
export 'visitroute.pb.dart';

class VisitRouteClient extends $grpc.Client {
  static final _$routeVisit =
      $grpc.ClientMethod<$0.RouteRequest, $0.RouteResponse>(
          '/visitroute.VisitRoute/RouteVisit',
          ($0.RouteRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.RouteResponse.fromBuffer(value));

  VisitRouteClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.RouteResponse> routeVisit($0.RouteRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$routeVisit, request, options: options);
  }
}

abstract class VisitRouteServiceBase extends $grpc.Service {
  $core.String get $name => 'visitroute.VisitRoute';

  VisitRouteServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.RouteRequest, $0.RouteResponse>(
        'RouteVisit',
        routeVisit_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RouteRequest.fromBuffer(value),
        ($0.RouteResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.RouteResponse> routeVisit_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RouteRequest> request) async {
    return routeVisit(call, await request);
  }

  $async.Future<$0.RouteResponse> routeVisit(
      $grpc.ServiceCall call, $0.RouteRequest request);
}
