///
//  Generated code. Do not modify.
//  source: visitroute.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class RouteRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RouteRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'visitroute'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'user')
    ..hasRequiredFields = false
  ;

  RouteRequest._() : super();
  factory RouteRequest({
    $core.String? user,
  }) {
    final _result = create();
    if (user != null) {
      _result.user = user;
    }
    return _result;
  }
  factory RouteRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RouteRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RouteRequest clone() => RouteRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RouteRequest copyWith(void Function(RouteRequest) updates) => super.copyWith((message) => updates(message as RouteRequest)) as RouteRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RouteRequest create() => RouteRequest._();
  RouteRequest createEmptyInstance() => create();
  static $pb.PbList<RouteRequest> createRepeated() => $pb.PbList<RouteRequest>();
  @$core.pragma('dart2js:noInline')
  static RouteRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RouteRequest>(create);
  static RouteRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get user => $_getSZ(0);
  @$pb.TagNumber(1)
  set user($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUser() => $_has(0);
  @$pb.TagNumber(1)
  void clearUser() => clearField(1);
}

class RouteResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RouteResponse', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'visitroute'), createEmptyInstance: create)
    ..pc<Visit>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: Visit.create)
    ..hasRequiredFields = false
  ;

  RouteResponse._() : super();
  factory RouteResponse({
    $core.Iterable<Visit>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory RouteResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RouteResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RouteResponse clone() => RouteResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RouteResponse copyWith(void Function(RouteResponse) updates) => super.copyWith((message) => updates(message as RouteResponse)) as RouteResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RouteResponse create() => RouteResponse._();
  RouteResponse createEmptyInstance() => create();
  static $pb.PbList<RouteResponse> createRepeated() => $pb.PbList<RouteResponse>();
  @$core.pragma('dart2js:noInline')
  static RouteResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RouteResponse>(create);
  static RouteResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Visit> get items => $_getList(0);
}

class Visit extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Visit', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'visitroute'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'label')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'target')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'code')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'lock')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'tag')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'realDay')
    ..hasRequiredFields = false
  ;

  Visit._() : super();
  factory Visit({
    $core.String? name,
    $core.String? label,
    $core.String? target,
    $core.String? code,
    $core.String? lock,
    $core.String? type,
    $core.String? tag,
    $core.String? realDay,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (label != null) {
      _result.label = label;
    }
    if (target != null) {
      _result.target = target;
    }
    if (code != null) {
      _result.code = code;
    }
    if (lock != null) {
      _result.lock = lock;
    }
    if (type != null) {
      _result.type = type;
    }
    if (tag != null) {
      _result.tag = tag;
    }
    if (realDay != null) {
      _result.realDay = realDay;
    }
    return _result;
  }
  factory Visit.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Visit.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Visit clone() => Visit()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Visit copyWith(void Function(Visit) updates) => super.copyWith((message) => updates(message as Visit)) as Visit; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Visit create() => Visit._();
  Visit createEmptyInstance() => create();
  static $pb.PbList<Visit> createRepeated() => $pb.PbList<Visit>();
  @$core.pragma('dart2js:noInline')
  static Visit getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Visit>(create);
  static Visit? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get label => $_getSZ(1);
  @$pb.TagNumber(2)
  set label($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLabel() => $_has(1);
  @$pb.TagNumber(2)
  void clearLabel() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get target => $_getSZ(2);
  @$pb.TagNumber(3)
  set target($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTarget() => $_has(2);
  @$pb.TagNumber(3)
  void clearTarget() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get code => $_getSZ(3);
  @$pb.TagNumber(4)
  set code($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCode() => $_has(3);
  @$pb.TagNumber(4)
  void clearCode() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get lock => $_getSZ(4);
  @$pb.TagNumber(5)
  set lock($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLock() => $_has(4);
  @$pb.TagNumber(5)
  void clearLock() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get type => $_getSZ(5);
  @$pb.TagNumber(6)
  set type($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasType() => $_has(5);
  @$pb.TagNumber(6)
  void clearType() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get tag => $_getSZ(6);
  @$pb.TagNumber(7)
  set tag($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasTag() => $_has(6);
  @$pb.TagNumber(7)
  void clearTag() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get realDay => $_getSZ(7);
  @$pb.TagNumber(8)
  set realDay($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasRealDay() => $_has(7);
  @$pb.TagNumber(8)
  void clearRealDay() => clearField(8);
}

