// Copyright (c) 2018, the gRPC project authors. Please see the AUTHORS file
// for details. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Dart implementation of the gRPC salesrep.Greeter client.
import 'package:grpc/grpc.dart';
import 'package:salesrep/src/auth/authsalesrep.pbgrpc.dart';
import 'package:salesrep/src/home/home.pbgrpc.dart';
import 'package:salesrep/src/tracking/tracking.pbgrpc.dart';
import 'package:salesrep/src/visit/visitroute.pbgrpc.dart';

import 'server.dart';

Future<void> main(List<String> args) async {
  await visitRoute();
  await performanceOfTheDay();
  await orderOfTheDay();
  await tracking();
  await auth();
}


Future visitRoute() async{
  final channel = ServeConfig.initializer();
  try {
    final stub = VisitRouteClient(channel);
    
    final req = RouteRequest();
    req..user = '08255859705';
    final response = await stub.routeVisit(req, options: CallOptions(compression: const GzipCodec()));

    print(response.items);
  } catch (e) {
    print(e);
  }
    await channel.shutdown();
}


Future performanceOfTheDay() async{
   final channel = ServeConfig.initializer();
  try {
    final stub = HomeClient(channel);
    final req = QuantityOfTheDayRequest();
    req ..user = '08255859705';
    final response = await stub.performanceOfTheDay(req, options: CallOptions(compression: const GzipCodec()));
    print(response.items);
  } catch (e) {
    print(e);
  }
}


Future orderOfTheDay() async{
   final channel = ServeConfig.initializer();
  try {
    final stub = HomeClient(channel);
    final req = OrderVolumeRequest();
    req ..user = '08255859705';
    final response = await stub.orderVolumeOfTheDayFMC(req, options: CallOptions(compression: const GzipCodec()));
    print(response.items);
  } catch (e) {
    print(e);
  }
}

Future tracking() async{
   final channel = ServeConfig.initializer();
   try {

     final stub = CisTrackingClient(channel);
     final req = TrackingRequest();
     req..user = '08255859705';
     final response = await stub.cis(req, options: CallOptions(compression: GzipCodec()));
     print(response.items);
     
   } catch (e) {
     print(e);
   }

}


Future auth() async {
  final channel = ServeConfig.initializer();
  try {
    final stub = AuthSalesRepClient(channel);
    final req = SignInRequest();
    req.username = '08255859705';
    req.password = '123456';
    final response = await stub.signInSalesRep(req, options: CallOptions(compression: GzipCodec()));
    if(response.user.isNotEmpty){
      final user = response.user['entity'];
      print('#################################');
      print('Olá ${user.name}, sejá bem vindo.');
      print('#################################');
    }else{
      print('Usuário ou senha inválidos!');
    }
  } catch (e) {
    print(e);
  }
}


class ServeConfig {
  static ClientChannel initializer() {
    final channel = ClientChannel(
      '192.168.0.13',
      port: 50051,
      options: ChannelOptions(
        credentials: ChannelCredentials.insecure(),
        codecRegistry:
            CodecRegistry(codecs: const [GzipCodec(), IdentityCodec()]),
      ),
    );
    return channel;
  }
}
