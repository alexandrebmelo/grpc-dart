// Copyright (c) 2018, the gRPC project authors. Please see the AUTHORS file
// for details. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// Dart implementation of the gRPC salesrep.Greeter server.

import 'package:grpc/grpc.dart';
import 'package:salesrep/src/auth/authsalesrep.pb.dart';
import 'package:salesrep/src/auth/authsalesrep.pbgrpc.dart';
import 'package:salesrep/src/home/home.pbgrpc.dart';
import 'package:salesrep/src/promotions/promotions.pbgrpc.dart';
import 'package:salesrep/src/tracking/tracking.pbgrpc.dart';
import 'package:salesrep/src/visit/visitroute.pbgrpc.dart';
import '../database/database.dart';

class VisitRoute extends VisitRouteServiceBase{

  @override
  Future<RouteResponse> routeVisit(ServiceCall call, RouteRequest request) async {
    print('Recebendo usuário - visita: ${request.user}');
    
    final obj = RouteResponse();
    for(var item in DataBase.routes){
     obj..items.add(item);
    }
    return obj;
  }
}


class Home extends HomeServiceBase {
  @override
  Future<QuantityOfTheDayResponse> performanceOfTheDay(ServiceCall call, QuantityOfTheDayRequest request) async {
    print('Recebendo usuário - performance do dia: ${request.user}');

    final obj = QuantityOfTheDayResponse();
    for(var item in DataBase.quantityOfTheDay){
      obj..items.add(item);
    }
    return obj;
  }

  @override
  Future<OrderVolumeResponse> orderVolumeOfTheDayFMC(ServiceCall call, OrderVolumeRequest request) async {
    print('Recebendo usuário - volume pedidos do dia: ${request.user}');

    final obj = OrderVolumeResponse();
    for(var item in DataBase.orderValumeOfTheday){
      obj..items.add(item);
    }
    return obj;
  }
}


class Tracking extends CisTrackingServiceBase{
  @override
  Future<TrackingResponse> cis(ServiceCall call, TrackingRequest request) async {
    
    print('Recebendo usuário - tracking de pedidos ${request.user}');

    final obj = TrackingResponse();
    for(var item in DataBase.trackings){
      obj.items.add(item);
    }
    return obj;
  }
}

class Promotions extends PromotionsServiceBase{
  @override
  Future<PackResponse> packSC(ServiceCall call, PackRequest request) async {
     print('Recebendo usuário - promoções ${request.user}');
    final obj = PackResponse();
    for(var item in DataBase.packs){
      obj.items.add(item);
    }
    return obj;
  }
}


class Authentication extends AuthSalesRepServiceBase{

  @override
  Future<SignInResponse> signInSalesRep(ServiceCall call, SignInRequest request) async {
    final username = request.username; 
    final password = request.password;
    print('Username: $username Password: $password');
    
    final resposne = DataBase.users.where((e) => e.sid == username && e.password == password);
    final obj = SignInResponse();
    if(resposne.isNotEmpty){
       obj.user['entity'] = resposne.first;
       return obj;
    }

    return obj;
  }


}

Future<void> main(List<String> args) async {
  final server = Server(
    [VisitRoute(), Home(), Tracking(), Promotions(), Authentication()],
    const <Interceptor>[],
    CodecRegistry(codecs: const [GzipCodec(), IdentityCodec()]),
  );
  await server.serve(port: 50051);
  print('Server listening on port ${server.port}');
}
