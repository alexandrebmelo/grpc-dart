import 'package:salesrep/src/auth/authsalesrep.pb.dart';
import 'package:salesrep/src/home/home.pb.dart';
import 'package:salesrep/src/promotions/promotions.pb.dart';
import 'package:salesrep/src/tracking/tracking.pb.dart';
import 'package:salesrep/src/visit/visitroute.pb.dart';

class DataBase {
  static final List<Visit> routes = [
    Visit(
        code: '0051276793',
        label: 'F',
        name: 'Panificação Mestre do Sabor LTDA',
        target: '2.670',
        lock: 'DE SOQ',
        tag: 'PLAN',
        type: 'NEGÓCIOS',
        realDay: '3.421'),
    Visit(
        code: '0051276794',
        label: 'D',
        name: 'Panela de Pedra e Padaria e Rest',
        target: '2.462',
        lock: 'Inadimplência',
        tag: 'PLAN',
        type: 'NEGÓCIOS',
        realDay: '3.422'),
    Visit(
        code: '0051276794',
        label: 'TS',
        name: 'Villar bar',
        target: '2.462',
        lock: 'Inadimplência',
        tag: 'PLAN',
        type: 'NEGÓCIOS',
        realDay: '3.422'),
    Visit(
        code: '0051276794',
        label: 'F',
        name: 'Maria e Manoel Mercearia',
        target: '2.462',
        lock: 'Inadimplência',
        tag: 'PLAN',
        type: 'NEGÓCIOS',
        realDay: '3.422'),
  ];


  static final List<QuantityOfTheDay> quantityOfTheDay = [
    QuantityOfTheDay(title: 'Visitas do dia', realized: 38.0, notRealized: -22.0, target: 60.0, percent: 63.0),
    QuantityOfTheDay(title: 'Ordens do dia', realized: 61.0, notRealized: -28.0, target: 89.0, percent: 69.0)
  ];


  static final List<OrderValumeOfTheday> orderValumeOfTheday = [
    OrderValumeOfTheday(title: 'Volume pedidos do dia',realized: 65.571, notRealized: -17.179, percent: 83.0, target: 82.750),
    OrderValumeOfTheday(title: 'Volume pedidos do dia',realized: 50.371, notRealized: -40.379, percent: 55.5, target: 90.750),
    OrderValumeOfTheday(title: 'Volume pedidos do dia',realized: 32.131, notRealized: -46.069, percent: 41.08, target: 78.200),
  ];


  static final List<Tracking> trackings = [
    Tracking(
      date: 'Oct 25th 2021', 
      name: 'Lucky Strike Ice Cube', 
      nf: '002620859-37', 
      status: 'delivery', 
      updated: 'Oct 25th 2021 - 15:50', 
      imageLink: 'https://containerproject.s3.sa-east-1.amazonaws.com/salesrep/icon_cigarret_sample.png', 
      quantity: '1 Carton',
      historyList: [
        DetailedHistory(date: 'Oct 26th 2021', status: 'Expired CIS'),
        DetailedHistory(date: 'Oct 28th 2021', status: 'Attempt 2 - CIS billed Estimated delivery Date: Oct 28th 2021'),
        DetailedHistory(date: 'Oct 28th 2021', status: 'Attempt 2 - CIS Delivery unsuccessful: 002820622-37'),
        DetailedHistory(date: 'Oct 29th 2021', status: 'Attempt 4 - CIS EXPUNGED'),
        DetailedHistory(date: 'Oct 30th 2021', status: 'Attempt 5 - CIS billed Estimated Delivery Date: Oct 31th 2021'),
        DetailedHistory(date: 'Oct 31th 2021', status: 'Attempt 5 - CIS Delivery NF: 002962842-37'),
      ]
    ),
    Tracking(
      date: 'Oct 25th 2021', 
      name: 'Bobina Térmica', 
      nf: '002620859-37', 
      status: 'expunged', 
      updated: 'Oct 25th 2021 - 15:50', 
      imageLink: 'https://containerproject.s3.sa-east-1.amazonaws.com/salesrep/image_bobina.png', 
      quantity: '2 Units',
      historyList: [
        DetailedHistory(date: 'Oct 26th 2021', status: 'Expired CIS'),
        DetailedHistory(date: 'Oct 28th 2021', status: 'Attempt 2 - CIS billed Estimated delivery Date: Oct 28th 2021'),
        DetailedHistory(date: 'Oct 28th 2021', status: 'Attempt 2 - CIS Delivery unsuccessful: 002820622-37'),
        DetailedHistory(date: 'Oct 29th 2021', status: 'Attempt 4 - CIS EXPUNGED'),
        DetailedHistory(date: 'Oct 30th 2021', status: 'Attempt 5 - CIS billed Estimated Delivery Date: Oct 31th 2021'),
        DetailedHistory(date: 'Oct 31th 2021', status: 'Attempt 5 - CIS Delivery NF: 002962842-37'),
      ]
    ),
    Tracking(
      date: 'Oct 25th 2021', 
      name: 'Papel Trevo com cola', 
      nf: '002620859-37', 
      status: 'confirmed', 
      updated: 'Oct 25th 2021 - 15:50', 
      imageLink: 'https://containerproject.s3.sa-east-1.amazonaws.com/salesrep/image_trevo.png', 
      quantity: '2 Units',
      historyList: [
        DetailedHistory(date: 'Oct 26th 2021', status: 'Expired CIS'),
        DetailedHistory(date: 'Oct 28th 2021', status: 'Attempt 2 - CIS billed Estimated delivery Date: Oct 28th 2021'),
        DetailedHistory(date: 'Oct 28th 2021', status: 'Attempt 2 - CIS Delivery unsuccessful: 002820622-37'),
        DetailedHistory(date: 'Oct 29th 2021', status: 'Attempt 4 - CIS EXPUNGED'),
        DetailedHistory(date: 'Oct 30th 2021', status: 'Attempt 5 - CIS billed Estimated Delivery Date: Oct 31th 2021'),
        DetailedHistory(date: 'Oct 31th 2021', status: 'Attempt 5 - CIS Delivery NF: 002962842-37'),
      ]
    )
  ];

  static final List<Pack> packs = [
    Pack(id: 0, imageLink: 'https://containerproject.s3.sa-east-1.amazonaws.com/salesrep/icon_example_promo_0.png',name: 'Pack 1'),
    Pack(id: 1, imageLink: 'https://containerproject.s3.sa-east-1.amazonaws.com/salesrep/icon_example_promo_1.png',name: 'Pack 2'),
    Pack(id: 2, imageLink: 'https://containerproject.s3.sa-east-1.amazonaws.com/salesrep/icon_example_promo_2.png',name: 'Pack 3'),
  ];

  static final List<UserSalesRep> users = [
    UserSalesRep(name: 'Alexandre Borges de Melo', email: 'amelo.borges@gmail.com',birthDate: '08/05/1978', phoneNumber: '+55 21 981238199', occupation: 'manager', b2bStatus: 'Active', sid: '08255859705', password: '123456'),
    UserSalesRep(name: 'Armando pinto', email: 'armando.pintos@gmail.com',birthDate: '08/05/2009', phoneNumber: '+55 21 999999999', occupation: 'manager', b2bStatus: 'Active', sid: '99999999999', password: '000000')
  ];
}
